/**
 * Hàm nhận vào 1 giá trị object hoặc array lưu vào localStorage của trình duyệt
 * @param {*} content là object hoặc array muốn lưu
 * @param {*} storeName là tên của storage
 */
function setStore(content, storeName) {
  var contenJSON = JSON.stringify(content);
  localStorage.setItem(storeName, contenJSON);
}

/**
 * Hàm lấy dữ liệu từ localstorage dựa vào storeName
 * @param {*} storeName tên store cần lấy dữ liệu
 * @returns trả về object hoặc array
 */
function getStore(storeName) {
  var output;
  if (localStorage.getItem(storeName)) {
    var output = JSON.parse(localStorage.getItem(storeName));
  }
  return output;
}

/**
 * Hàm này sẽ nhận vào 1 array (nhanVien) và trả ra output là string <tr>....</tr>
 *
 * @param {*} arrNhanVien arrSinhVien là mảng các object sinhVien [nhanVien1,nhanVien2,...]
 * @returns trả ra 1 giá trị là 1 htmlString '<tr>...</tr> <tr>...</tr>'
 */
function renderNhanVien(arrNhanVien) {
  var html = "";
  for (var i = 0; i < arrNhanVien.length; i++) {
    var item = arrNhanVien[i];
    var nv = new NhanVien(
      item.tknv,
      item.name,
      item.email,
      item.password,
      item.datePicker,
      item.luongCB,
      item.chucVu,
      item.gioLam
    );
    html += `
      <tr>
        <td>${nv.tknv}</td>
        <td>${nv.name}</td>
        <td>${nv.email}</td>
        <td>${nv.datePicker}</td>
        <td>${nv.chucVu}</td>
        <td>${nv.tongLuong()}</td>
        <td>${nv.xepLoai()}</td>
        <td>
            <button class="btn btn-danger" onclick="xoaNhanVien(${
              nv.tknv
            })">Xoá</button>
        </td>
        <td>
        <button class="btn btn-success" onclick="ThongTinNVCanSua(${
          nv.tknv
        })" data-toggle="modal" data-target="#myModal">sửa</button>
    </td>
      </tr>`;
  }
  return html;
}

function xoaNhanVien(maNVCanXoa) {
  console.log(maNVCanXoa);
  var vitricanxoa = timKiemVitri(mangNhanVien, maNVCanXoa);
  console.log("timKiemVitri: ", vitricanxoa);
  if (vitricanxoa !== -1) {
    mangNhanVien.splice(vitricanxoa, 1);
    var html = renderNhanVien(mangNhanVien);
    document.querySelector("tbody").innerHTML = html;
    setStore(mangNhanVien, "mangNhanVien");
  }
}
/**
 * Hàm này đưa vào mã nhân viên sẽ trả về vị trí nhân viên trong danh sách
 * @param {*} maNV mã nhân viên
 * @returns vị trí nhân viên
 */
function timKiemVitri(mangNhanVien, maNV) {
  for (var i = 0; i < mangNhanVien.length; i++) {
    var nv = mangNhanVien[i];
    if (nv.tknv == maNV) {
      return i;
    }
  }
}

function layThongTinTuForm() {
  var nhanVien = new NhanVien();
  nhanVien.tknv = document.getElementById("tknv").value;
  nhanVien.name = document.getElementById("name").value;
  nhanVien.email = document.getElementById("email").value;
  nhanVien.password = document.getElementById("password").value;
  nhanVien.datePicker = document.getElementById("datepicker").value;
  nhanVien.luongCB = document.getElementById("luongCB").value * 1;
  nhanVien.chucVu = document.getElementById("chucvu").value;
  nhanVien.gioLam = document.getElementById("gioLam").value * 1;
  return nhanVien;
}
