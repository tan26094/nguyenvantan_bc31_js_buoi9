/**
 * Hàm kiểm tra rỗng dựa vào giá trị người dùng nhập
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ (true) hoặc không hợp lệ (false);
 */
function kiemTraRong(value, selectorError, name) {
  if (value.toString() === "" || value.toString() === "0") {
    document.querySelector(selectorError).style.display = "block";
    document.querySelector(selectorError).innerHTML =
      name + "không được bỏ trống!";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "hh";
  document.querySelector(selectorError).style.display = "none";
  return true;
}

/**
 * Hàm kiểm tra rỗng dựa vào giá trị người dùng nhập
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ (true) hoặc không hợp lệ (false);
 */
function kiemTraMaNhanVien(value, selectorError, name) {
  var regexNumber = /^[0-9]+$/;
  if (regexNumber.test(value) && value.length >= 4 && value.length <= 6) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).style.display = "block";
  document.querySelector(selectorError).innerHTML =
    name + " chỉ bao gồm số, độ dài từ 3-6 ký số";
  return false;
}
function kiemTraTenNhanVien(value, selectorError, name) {
  var regexNumber = /^[A-Z a-z]+$/;
  if (regexNumber.test(value) && value.length >= 1) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).style.display = "block";
  document.querySelector(selectorError).innerHTML =
    name + " chỉ bao gồm ký tự, và không được để trống";
  return false;
}

function kiemTraEmail(value, selectorError, name) {
  var regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regexEmail.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).style.display = "block";
  document.querySelector(selectorError).innerHTML =
    name + " không đúng định dạng !";
  return false;
}

function kiemTraPassword(value, selectorError, name) {
  var regexPW =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
  console.log(value);
  console.log("regexPW.test(value): ", regexPW.test(value));
  if (regexPW.test(value)) {
    document.querySelector(selectorError).style.display = "block";
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).style.display = "block";
  document.querySelector(selectorError).innerHTML =
    name +
    " tối thiểu có 8 ký tự, bao gồm ít nhất 1 chữ hoa, 1 chữ thường và 1 ký tự đặt biệt !";
  return true;
}
function kiemTraLuong(value, selectorError, name) {
  if (value >= 1000000 && value <= 20000000) {
    document.querySelector(selectorError).style.display = "block";
    document.querySelector(selectorError).innerHTML = "";
    return false;
  }
  document.querySelector(selectorError).style.display = "block";
  document.querySelector(selectorError).innerHTML =
    name + "không được bỏ trống!, và từ 1 triệu đến 20 triệu";
  return true;
}
function kiemTraGio(value, selectorError, name) {
  if (value >= 80 && value <= 200) {
    document.querySelector(selectorError).style.display = "block";
    document.querySelector(selectorError).innerHTML = "";
    return false;
  }
  document.querySelector(selectorError).style.display = "block";
  document.querySelector(selectorError).innerHTML =
    name + "không được bỏ trống!, và từ 80 đến 200 giờ";
  return true;
}
