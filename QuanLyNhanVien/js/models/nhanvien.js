function NhanVien(
  _tknv,
  _name,
  _email,
  _password,
  _datePicker,
  _luongCB,
  _chucVu,
  _gioLam
) {
  this.tknv = _tknv;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.datePicker = _datePicker;
  this.luongCB = _luongCB;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tongLuong = function () {
    console.log(this.chucVu);
    if (this.chucVu == "Giám đốc") {
      return this.luongCB * 3;
    }
    if (this.chucVu == "Trưởng phòng") {
      return this.luongCB * 2;
    }
    if (this.chucVu == "Nhân viên") {
      return this.luongCB * 1;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất xắc";
    }
    if (this.gioLam >= 176) {
      return "Giỏi";
    }
    if (this.gioLam >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
