var mangNhanVien = [];

// B1: Khi người dùng nhấn vào nút xác nhận thì bắt đầu lấy thông tin gán vào object
document.getElementById("btnThemNV").onclick = function () {
  document.getElementById("tknv").disabled = 0;
  document.getElementById("btnThemNV").style.visibility = "visible";
  var nhanVien = layThongTinTuForm();
  // Validation
  var valid = true;
  //Kiểm tra dữ liệu trước khi thêm vào mảng
  valid &=
    kiemTraRong(nhanVien.tknv, "#tbTKNV", "Mã nhân viên ") &
    kiemTraRong(nhanVien.name, "#tbTen", "Tên nhân viên ") &
    kiemTraRong(nhanVien.email, "#tbEmail", "email ") &
    kiemTraRong(nhanVien.luongCB, "#tbLuongCB", "Lương cơ bản ") &
    kiemTraRong(nhanVien.chucVu, "#tbChucVu", "Chức vụ ") &
    kiemTraRong(nhanVien.gioLam, "#tbGioLam", "Giờ làm ") &
    kiemTraMaNhanVien(nhanVien.tknv, "#tbTKNV", "Mã nhân viên ") &
    kiemTraTenNhanVien(nhanVien.name, "#tbTen", "Tên nhân viên ") &
    kiemTraEmail(nhanVien.email, "#tbEmail", "Email nhân viên ") &
    kiemTraPassword(nhanVien.password, "#tbMatKhau", "Password") &
    kiemTraLuong(nhanVien.luongCB, "#tbLuongCB", "Lương cơ bản") &
    kiemTraGio(nhanVien.gioLam, "#tbGioLam", "Giờ làm");

  // //Kiểm tra ký tự
  // valid &= kiemTraTatCaKyTu(sinhVien.tenSinhVien,'#error_all_letter_tenSinhVien', 'Tên sinh viên');

  // //Kiểm tra email
  // valid &= kiemTraEmail(sinhVien.email,'#error_email','email');

  // //Kiểm tra số điện thoại
  // valid &= kiemTraSo(sinhVien.soDienThoai,'#error_all_number_soDienThoai','Số điện thoại');

  if (!valid) {
    return;
  }
  mangNhanVien.push(nhanVien);
  // Lưu vào local storage
  setStore(mangNhanVien, "mangNhanVien");
  // Render HTML
  var html = renderNhanVien(mangNhanVien);
  document.querySelector("tbody").innerHTML = html;
};

window.onload = function () {
  //Lấy giữ liệu từ store
  var content = getStore("mangNhanVien");
  if (content) {
    mangNhanVien = content;
    var html = renderNhanVien(content);
    document.querySelector("tbody").innerHTML = html;
  }
};

document.getElementById("btnCapNhat").onclick = function () {
  var nhanvien = layThongTinTuForm();
  var viTriNhanVienCanSua = timKiemVitri(mangNhanVien, nhanvien.tknv);
  mangNhanVien[viTriNhanVienCanSua] = nhanvien;
  var html = renderNhanVien(mangNhanVien);
  document.querySelector("tbody").innerHTML = html;
  setStore(mangNhanVien, "mangNhanVien");
};

function ThongTinNVCanSua(maNVCanSua) {
  document.getElementById("tknv").disabled = 1;
  document.getElementById("btnThemNV").style.visibility = "hidden";
  var viTriNhanVienCanSua = timKiemVitri(mangNhanVien, maNVCanSua);
  nv = mangNhanVien[viTriNhanVienCanSua];
  console.log("nv: ", nv);
  document.getElementById("tknv").value = nv.tknv;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.password;
  document.getElementById("datepicker").value = nv.datePicker;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
